# FISAS CI/CD Pipelines

This repository holds all the necessary information to understand and apply GitLab CI pipelines to FI/SAS repositories.

## GitLab CI configuration

Since the FI/SAS project is micro-service based, spanning multiple repositories following a set of rules and configuration, the best solution is to centralize all common features in one place (this repository) overriding some exception only when needed.

For that purpose, this repository holds the common GitLab CI configurations to all repositories, which can be of two types:

 - **Micro-Services:** Specifications declared in [gitlab-ci.yml](gitlab-ci.yml)
 - **AngularJS:** Declared in [frontends.gitlab-ci.yml](frontends.gitlab-ci.yml)

The above configurations prepare the respositories to run jobs on the following pipeline stages:

 - **lint:** Run language specific *code-style* validation jobs
 - **test:** All necessary integrity tests, from unit tests to E2E tests
 - **release:** Generate the next automatic release and push it to GitLab as a new Tag. See [Semantic-release](#semantic-release)
 - **registry:** (*Micro-Services only*) Publish a Docker image, based in the new generated version, to be available for deploy
 - **staging:** (*AngularJS only*) Generate a Docker image to be used in staging environments
 - **production:** (*AngularJS only*) Generate a Docker image to be used in production

A Gitlab CI stage flow-chart using the above stages presents itself as:

```mermaid
graph LR;
  lint-->test;
  test-->build;
  build-->release;
  release-->registry;
  registry-->staging;
  registry-->production;
```

The remaining files available in the repository are:

 1. [docker-registry.gitlab-ci.yml](docker-registry.gitlab-ci.yml): Defines all Docker registry related jobs, icluding the base job to generate and push Docker images to the private GitLab Docker Registry of each repository. Such jobs are defined in a separate file to be properly included when needed.
 2. [release.gitlab-ci.yml](release.gitlab-ci.yml): Provides the *semantic-release* base job to be used as-is or extended by certain repositories

## Semantic-release

Using [semantic-release](https://github.com/semantic-release/semantic-release), the next application version is calculated based on *commit* messages (following a given standard, in this case the AngularJS standard) and automatically published to multiple outputs.

The *semantic-release* features used for this project must be defined on each repository through the`.releaserc.yml` file, having the following content:

```yaml
plugins:
- "@semantic-release/commit-analyzer"
- "@semantic-release/release-notes-generator"
- "@semantic-release/changelog"
- - "@semantic-release/npm"
  - npmPublish: false
- - "@semantic-release/git"
  - assets:
    - CHANGELOG.md
    - package.json
    - package-lock.json
    message: "chore(release): version [skip ci]${nextRelease.version}\n\n${nextRelease.notes}"
- "@semantic-release/gitlab"

branches:
- "master"
- "+([0-9])?(.{+([0-9]),x}).x"
- name: "develop"
  prerelease: "rc"
- name: "test-ci"
  prerelease: true
```
This configuration generates the following actions in the GitLab repository:

 1. On the *master* branch, and every branch matching a specific version, a new release will be generated with *production-ready* code
 2. On the branch *develop* and *test-ci*, a *staging-ready* version will be generated (both have distinguished release notations to avoid test version overlap
 3. The new version will be bundled into a GitLab repository Tag, with the compiled release notes and tag number matching the calculated version

Certain open-source packages from this project are also published to the public NPM registry, i.e. [Base code for micro-services](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer) which pushes the new generated version to: https://www.npmjs.com/package/@fisas/ms_core

## Husky git-hook

To insure that all dependencies are up-to-date when a new version is deployed to the git repository, a validation shell script is executed to verifiy if the `package-lock.json` file was updated along with the `package.json`.

Applying this hook is straightforward using [Husky](https://typicode.github.io/husky/#/), by adding the following rules in the `package.json` file specification:

```javascript
"husky": {
	"hooks": {
		"pre-commit": ".githooks/pre-commit/check-package-lock.sh",
	}
},
```
Which executes the shell script existing in the path: `.githooks/pre-commit/check-package-lock.sh`, having the following contents:

```bash
#!/bin/bash

function  modifiedFile {
	git diff --name-only HEAD | grep "^$1" > /dev/null 2>&1
}
 
if modifiedFile 'package.json' && ! modifiedFile 'package-lock.json' ; then
	echo  "ERROR: package.json modified but no package-lock.json staged to commit."
	exit 1
fi
```

## FAQs

- [Where are the GitLab CI, NPM and Docker Registry secrets stored?](#where-are-the-gitlab-ci-npm-and-docker-registry-secrets-stored)
- [What files must be present to apply these pipelines?](#what-files-must-be-present-to-apply-these-pipelines)


### Where are the GitLab CI, NPM and Docker Registry secrets stored
All necessary credentials, tokens and sensitive data are globally declared as variables in the GitLab Group-level CI/CD configurations, to be safely stored and used across different repositories

### What files must be present to apply these pipelines?
#### `.gitlab-ci.yml`
The primary file to look for in any repository is the `.gitlab-ci.yml`, where all pipelines are specified. These pipelines aim to simplify the pipeline declaration on every repository, following a DRY-*esque* methodology, such as:

```yaml
include:
  - 'https://gitlab.com/fi-sas/cicd/-/raw/master/gitlab-ci.yml'
  - 'https://gitlab.com/fi-sas/cicd/-/raw/master/release.gitlab-ci.yml'

stages:
  - lint
  - test
  - release
  - registry
  - staging
  - production
```
All the necessary step are to include the centralized jobs defined in this repository and declare the needed pipeline-stages.

#### `.githooks/pre-commit/check-package-lock.sh`

See [Husky git-hook](#husky-git-hook)

#### `.releaserc.yml`

See the [Semantic-release](#semantic-release) section above
